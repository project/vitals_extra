# Vitals Extra

This module extends the [Vitals](https://www.drupal.org/project/vitals) module 
with additional plugins and functionality.

## Requirements
This module requires the contrib [Vitals](https://www.drupal.org/project/vitals) (2.2.x or higher) module.

## Installation
Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/docs/extending-drupal Installing modules for 
further information.

```shell
composer require drupal/vitals_extra
drush en vitals_extra
```

## Extra Vitals plugins
This module provides the following extra plugins for Vitals.
You can enabled them individually on the Vitals configuration page
(/admin/config/services/vitals).

### Update status
Returns the update check interval, a list with e-mail addresses that
receive the update messages, and an e-mail status boolean (when there's
one or more e-mail addessses, it returns `true`).

```json
"update_status":{
  "interval": 7,
  "emails_list": [],
  "emails_status": false
},
```

### Environment indicator
Returns the name of the current environment, if the [Environment Indicator](https://www.drupal.org/project/environment_indicator) module is enabled.

When the [current_release state](https://git.drupalcode.org/project/environment_indicator/-/blob/4.x/README.md#configuration) is set, then it will also return the release value.

```json
"environment_indicator":{
  "name": "current environment name",
  "release": "v1.2.44"
},
```

### Development modules
Returns the status for common development modules ([reroute_email](https://www.drupal.org/project/reroute_email) (or [symfony_mailer_reroute](https://www.drupal.org/project/symfony_mailer_reroute)), [shield](https://www.drupal.org/project/shield), [devel](https://www.drupal.org/project/devel), and [stage_file_proxy](https://www.drupal.org/project/stage_file_proxy)).

```json
"dev_modules": {
  "devel": false,
  "stage_file_proxy": false,
  "shield": false,
  "reroute_email": false
},
```

**Note:** reroute_email will be `true` if the reroute_email or 
symfony_mailer_reroute modules are enabled.

### Mail settings
Returns the provider & transport of the website e-mail settings. Should be compatible with [symfony_mailer](https://www.drupal.org/project/symfony_mailer), [swiftmailer](https://www.drupal.org/project/swiftmailer), [mailsystem](https://www.drupal.org/project/mailsystem), [smtp](https://www.drupal.org/project/smtp), [phpmailer_smtp](https://www.drupal.org/project/phpmailer_smtp).

```json
"mail": {
  "provider": "symfony_mailer",
  "transport": "smtp"
},
```

## LINKS
* Project page: https://www.drupal.org/project/vitals_extra
* Issues: https://www.drupal.org/project/issues/vitals_extra
