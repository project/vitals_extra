<?php

namespace Drupal\vitals_extra\Plugin\VitalsCheck;

use Drupal\Core\State\StateInterface;
use Drupal\vitals\VitalsCheckPluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the vitals_check.
 *
 * Checking the current environment.
 *
 * @VitalsCheck(
 *   id = "environment_indicator",
 *   label = @Translation("Environment indicator"),
 *   description = @Translation("Returns the environment name and release from in the Environment Indicator module.")
 * )
 */
class EnvironmentIndicator extends VitalsCheckPluginBase implements ContainerFactoryPluginInterface {

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */

  protected $configFactory;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */

  protected $moduleHandler;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\State\StateInterface $state
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StateInterface $state, ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
    $this->state = $state;

  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('state'),
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $output = [
      'name' => NULL,
      'release' => NULL,
    ];

    if ($this->moduleHandler->moduleExists('environment_indicator')) {
      $output['name'] = $this->configFactory->get('environment_indicator.indicator')->get('name');
    }
    if ($this->state->get('environment_indicator.current_release')) {
      $output['release'] = $this->state->get('environment_indicator.current_release');
    }

    return $output;
  }

}
