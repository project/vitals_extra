<?php

namespace Drupal\vitals_extra\Plugin\VitalsCheck;

use Drupal\vitals_extra\VitalsExtraPlugin;

/**
 * Plugin implementation of the vitals_check for checking the update settings.
 *
 * @VitalsCheck(
 *   id = "update_status",
 *   label = @Translation("Update status"),
 *   description = @Translation("Returns the update check interval and e-mail settings.")
 * )
 */
class UpdateStatus extends VitalsExtraPlugin {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $interval = $this->configFactory->get('update.settings')->get('check.interval_days');
    $emails_list = [];

    // Check if there is an e-mail set for the core update settings.
    if (!empty($this->configFactory->get('update.settings')->get('notification.emails'))) {
      $emails = $this->configFactory->get('update.settings')->get('notification.emails');
      foreach ($emails as $email) {
        $emails_list[] = $email;
      }
    };

    // If symfony_mailer is used, then get check if there are e-mails
    // set for the update policy, otherwise get the default settings.
    if (\Drupal::moduleHandler()->moduleExists('symfony_mailer')) {
      if (!empty($this->configFactory->get('symfony_mailer.mailer_policy.update.status_notify')->get('configuration.email_to.addresses'))) {
        $emails = $this->configFactory->get('symfony_mailer.mailer_policy.update.status_notify')->get('configuration.email_to.addresses');
        foreach ($emails as $email) {
          $emails_list[] = $email['value'];
        }
      }
      elseif (!empty($this->configFactory->get('symfony_mailer.mailer_policy._')->get('configuration.email_to.addresses'))) {
        $emails = $this->configFactory->get('symfony_mailer.mailer_policy._')->get('configuration.email_to.addresses');
        foreach ($emails as $email) {
          $emails_list[] = $email['value'];
        }
      }
    }

    // If there are one of more e-mails available,
    // set a general boolean to true.
    $emails_send = FALSE;
    if (!empty($emails_list)) {
      $emails_send = TRUE;
    }

    $output = [
      'interval' => $interval,
      'emails_list' => $emails_list,
      'emails_status' => $emails_send,
    ];

    return $output;
  }

}
