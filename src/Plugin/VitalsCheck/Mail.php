<?php

namespace Drupal\vitals_extra\Plugin\VitalsCheck;

use Drupal\vitals_extra\VitalsExtraPlugin;

/**
 * Plugin implementation of the vitals_check for checking the mail.
 *
 * Settings of the site.
 *
 * @VitalsCheck(
 *   id = "mail",
 *   label = @Translation("Mail settings"),
 *   description = @Translation("Returns general mail settings of the website.")
 * )
 */
class Mail extends VitalsExtraPlugin {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $output = [
      'provider' => NULL,
      'transport' => NULL,
    ];

    // Mailsystem (required by swiftmailer, optional for smtp & phpmailer_smtp)
    if ($this->moduleHandler->moduleExists('mailsystem')) {
      $mailsystem_sender = $this->configFactory->get('mailsystem.settings')->get('defaults.sender');
      $output['provider'] = 'mailsystem > ' . $mailsystem_sender;
      $output['transport'] = $mailsystem_sender;

      if ($mailsystem_sender == 'swiftmailer') {
        $output['transport'] = $this->configFactory->get('swiftmailer.transport')->get('transport');
      }
      elseif ($mailsystem_sender == 'smtp' || $mailsystem_sender == 'phpmailer_smtp') {
        $output['transport'] = 'smtp';
      }
    }
    // Symfony mailer.
    elseif ($this->moduleHandler->moduleExists('symfony_mailer')) {
      $output['provider'] = 'symfony_mailer';
      $output['transport'] = $this->configFactory->get('symfony_mailer.settings')->get('default_transport');
    }
    // Smtp (without mailsystem)
    elseif ($this->moduleHandler->moduleExists('smtp') && !empty($this->configFactory->get('smtp.settings')->get('smtp_on'))) {
      $output['provider'] = 'smtp';
      $output['transport'] = 'smtp';
    }
    // phpmailer_smtp (without mailsystem)
    elseif ($this->moduleHandler->moduleExists('phpmailer_smtp')) {
      $output['provider'] = 'phpmailer_smtp';
      $output['transport'] = 'smtp';
    }
    // Default settings.
    else {
      $output['provider'] = $this->configFactory->get('system.mail')->get('interface.default');
      $output['transport'] = $this->configFactory->get('system.mail')->get('interface.default');
    }

    return $output;
  }

}
