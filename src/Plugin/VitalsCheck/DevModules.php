<?php

namespace Drupal\vitals_extra\Plugin\VitalsCheck;

use Drupal\vitals_extra\VitalsExtraPlugin;

/**
 * Plugin implementation of the vitals_check for checking the status.
 *
 * @VitalsCheck(
 *   id = "dev_modules",
 *   label = @Translation("Development modules"),
 *   description = @Translation("Returns the status for common development modules (reroute_email/symfony_mailer_reroute, shield, devel, stage_file_proxy, shield).")
 * )
 */
class DevModules extends VitalsExtraPlugin {

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $output = [
      'devel' => $this->moduleHandler->moduleExists('devel'),
      'stage_file_proxy' => FALSE,
      'shield' => FALSE,
      'reroute_email' => FALSE,
    ];

    if ($this->moduleHandler->moduleExists('stage_file_proxy') && !empty($this->configFactory->get('stage_file_proxy.settings')->get('origin'))) {
      $output['stage_file_proxy'] = TRUE;
    }

    if ($this->moduleHandler->moduleExists('shield') && !empty($this->configFactory->get('shield.settings')->get('shield_enable'))) {
      $output['shield'] = TRUE;
    }

    if ($this->moduleHandler->moduleExists('reroute_email')) {
      $output['reroute_email'] = $this->configFactory->get('reroute_email.settings')->get('enable');
    }
    elseif ($this->moduleHandler->moduleExists('symfony_mailer_reroute')) {
      $output['reroute_email'] = $this->configFactory->get('symfony_mailer_reroute.settings')->get('enable');
    }

    return $output;
  }

}
